import java.util.Scanner;
public class Conditionals {
public static void main(String[]args){
	System.out.print("What is your age");
	Scanner scan = new Scanner(System.in);
	int age = scan.nextInt();
	if (age>17){
		System.out.println("You can drive");
	}else if(age==16){
		System.out.println("You can drive with a permit");
	}else{
		System.out.println("You are not old enough");
	}
		
}	
}
