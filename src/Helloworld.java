
public class Helloworld {
	public static void main(String[]args){
		System.out.println("hello world");
		int age = 10;
		String name = "Morgan";
		String color = "red";
		String sport = "tennis";
		String animal = "dog";
		String season = "summer";
		System.out.println("My name is "+name);
		System.out.println("My favorite color is "+color);
		System.out.println("I like to play "+ sport);
		System.out.println("I have a "+ animal);
		System.out.println("I love the "+ season);
	}
}
